describe('PromiseQ Test Suite', () => {
    it('task1', () => {
      // Visit the promiseq website
      cy.visit('https://app.promiseq.com/login')

      //login
      cy.get('#email').type("chamodidilshani98@gmail.com")
      cy.get('#password').type("Akcd@12345")
      cy.get('.login-btn').click()

      cy.wait(6000)
      cy.get('.row.d-flex').should('be.visible')

      // Test navigation
      cy.get('.d-flex > .v-item-group > :nth-child(1)').click()
      cy.wait(6000)
      cy.url().should('eq', 'https://app.promiseq.com/sandbox')

      cy.get('.v-item-group > :nth-child(4)').click()
      cy.wait(6000)
      cy.url().should('eq','https://app.promiseq.com/alarms')

      cy.get('.v-item-group > :nth-child(6)').click()
      cy.wait(6000)
      cy.url().should('eq','https://app.promiseq.com/live')

      cy.get('.v-item-group > :nth-child(8)').click()
      cy.wait(6000)
      cy.url().should('eq','https://app.promiseq.com/maps')

      cy.get('.v-item-group > :nth-child(10)').click()
      cy.wait(6000)
      cy.url().should('eq','https://app.promiseq.com/statistics')

      cy.get('.v-item-group > :nth-child(12)').click()
      cy.wait(6000)
      cy.url().should('eq','https://app.promiseq.com/configurations')

      cy.get('.v-item-group > :nth-child(14)').click()
      cy.wait(6000)
      cy.url().should('eq','https://app.promiseq.com/user-management')

    })
  })