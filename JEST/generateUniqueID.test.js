const { generateUniqueID } = require('./generateUniqueID.js');

test('generateUniqueID generates an ID', () => {

  const ID = generateUniqueID();

  if (ID) {
    console.log('Generated ID:', ID);
  } else {
    console.log('Failed to generate ID');
  }
});

